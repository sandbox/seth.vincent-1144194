<?php

function news_network_form_system_theme_settings_alter(&$form, $form_state) {

  $form['network_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Network settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );

  $form['network_settings']['short_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site short name'),
    '#size' => 15,
    '#description' => t("The short version of this site's name. If the site name is, say, 'Olympia, Washington' you would want to enter something like 'Olympia' here."),
    '#default_value' => theme_get_setting('short_name'),
  );
  
  $form['network_settings']['network'] = array(
    '#type' => 'textfield',
    '#title' => t('Network'),
    '#size' => 15,
    '#description' => t("The network to which this site belongs."),
    '#default_value' => theme_get_setting('network'),
  );
  
  $form['network_settings']['network_short_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Network Short Name'),
    '#size' => 15,
    '#description' => t("The short name of the network to which this site belongs."),
    '#default_value' => theme_get_setting('network_short_name'),
  );
  
  $form['network_settings']['network_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Network url'),
    '#size' => 15,
    '#description' => t("The link to the network to which this site belongs. <b>Make sure to include http://</b>"),
    '#default_value' => theme_get_setting('network_url'),
  );
  
  return $form;

}
