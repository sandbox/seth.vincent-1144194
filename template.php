<?php

function news_network_preprocess(&$vars) {
  $vars['short_name'] = theme_get_setting('short_name');
  $vars['network'] = theme_get_setting('network');
  $vars['network_short_name'] = theme_get_setting('network_short_name');
  $vars['network_url'] = theme_get_setting('network_url');
}
